"""
 Gugal test engine
"""

# about
about = {
    "website": "https://gitlab.com/gugal/searx-test",
    "official_api_documentation": None,
    "use_official_api": False,
    "require_api_key": False,
    "results": 'HTML'
}

# engine dependent config
categories = ['general']
paging = True
engine_type = "offline"
display_error_messages = True
name = "gugal-test"
engine = "gugal_test_engine"
shortcut = "gu"

# do search-request
def search(query, params):
    results = []

    if query == "!onlyResultKey":
        return [{"result":"a result"}]

    for i in range(0,10):
        results.append({
            'url': f"https://gitlab.com/narektor/gugal?dummyArgument={i}",
            'title': f"{query} #{i+1} lorem ipsum",
            "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dictum felis eu nulla lobortis, eu lacinia odio luctus. Donec iaculis tempor magna vitae ultrices. Quisque."
        })

    return results
