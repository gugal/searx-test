# Test engine for searx

A test searx "search engine" that returns predictable results.

# This is not a Gugal SERP provider

Rather, this is used to test Gugal and the searx SERP provider without requiring any API keys or getting unpredictable results due to search results changing